package com.kosign.tablayoutdemo.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kosign.tablayoutdemo.R;

public class CardFragment extends Fragment {
    private static CardFragment instance = null;
    public static CardFragment getInstance() {
        if (instance == null) {
            instance = new CardFragment();
            return instance;
        } else {
            return instance;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.card_fragment, container, false);
        return view;
    }
}
