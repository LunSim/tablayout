package com.kosign.tablayoutdemo.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kosign.tablayoutdemo.R;

public class NewsFragment extends Fragment {
    private static NewsFragment instance = null;
    public static NewsFragment getInstance() {
        if (instance == null) {
            instance = new NewsFragment();
            return instance;
        } else {
            return instance;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_fragment, container, false);
        return view;
    }
}
