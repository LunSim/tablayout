package com.kosign.tablayoutdemo;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.kosign.tablayoutdemo.fragment.CardFragment;
import com.kosign.tablayoutdemo.fragment.NewsFragment;
import com.kosign.tablayoutdemo.fragment.NotificationFragment;
import com.kosign.tablayoutdemo.fragment.SettingFragment;

public class MyAdapter extends FragmentPagerAdapter {
    private int numberOfFragments;

    public MyAdapter(@NonNull FragmentManager fm, int numberOfFragments) {
        super(fm, numberOfFragments);
        this.numberOfFragments = numberOfFragments;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return CardFragment.getInstance();
            case 1:
                return NewsFragment.getInstance();
            case 2:
                return NotificationFragment.getInstance();
            default:
                return SettingFragment.getInstance();
        }
    }

    @Override
    public int getCount() {
        return numberOfFragments;
    }
}
