package com.kosign.tablayoutdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,ViewPager.OnPageChangeListener {
    private Toolbar mToolbar;
    private ViewPager mViewpager;
    private RelativeLayout rl_MyCard,rl_News,rl_Notification,rl_Setting;
    private ImageView mMyCard,mMyCard_highligh;
    private ImageView mNews,mNews_highligh;
    private ImageView mNotification,mNotification_highligh;
    private ImageView mSetting,mSetting_highligh;

    private MyAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //init view
        initView();
        //init toolbar
        //initToolbar();
    }
    public void initView(){
        //mToolbar          = findViewById(R.id.toolbar);
        mViewpager        = findViewById(R.id.viewpager);

        mMyCard           = findViewById(R.id.myCard);
        mMyCard_highligh  = findViewById(R.id.iv_highligh_mycard);
        rl_MyCard         = findViewById(R.id.rl_myCard);

        mNews             = findViewById(R.id.myNews);
        mNews_highligh    = findViewById(R.id.iv_highligh_news);
        rl_News           = findViewById(R.id.rl_news);

        mNotification     = findViewById(R.id.myNotification);
        mNotification_highligh  = findViewById(R.id.iv_highligh_notification);
        rl_Notification   = findViewById(R.id.rl_notification);

        mSetting          = findViewById(R.id.mySetting);
        mSetting_highligh = findViewById(R.id.iv_highligh_setting);
        rl_Setting        = findViewById(R.id.rl_setting);

        /*register event*/
        initEventCnClick();
        initViewpager();

    }

    public void initEventCnClick(){
        rl_MyCard.setOnClickListener(this);
        rl_News.setOnClickListener(this);
        rl_Notification.setOnClickListener(this);
        rl_Setting.setOnClickListener(this);
    }

    public void initViewpager(){
        //mViewpager.setOffscreenPageLimit(4);
        //mAdapter = new MyAdapter(getSupportFragmentManager(),4);
        mViewpager.setAdapter(new MyAdapter(getSupportFragmentManager(),4));
        mViewpager.addOnPageChangeListener(this);
        // set selected fragment
        setSelectedTab(R.id.rl_myCard);
    }

    /*public void initToolbar(){
        mToolbar.setTitle(R.string.title_toolbar);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.myCard:
                mViewpager.setCurrentItem(0);
                break;
            case R.id.myNews:
                mViewpager.setCurrentItem(1);
                break;
            case R.id.myNotification:
                mViewpager.setCurrentItem(2);
                break;
            case R.id.mySetting:
                mViewpager.setCurrentItem(3);
                break;
        }
        setSelectedTab(v.getId());
    }

    /**
     * setVisible clicked on icons
     * */
    public void setSelectedTab(int id){
        if (id == R.id.rl_myCard){
            /**
             * show visible on icon
             * */
            mMyCard.setSelected(true);
            mNews.setSelected(false);
            mNotification.setSelected(false);
            mSetting.setSelected(false);

            /**
             * show border_button underline
             * */
            mMyCard_highligh.setVisibility(View.VISIBLE);
            mNews_highligh.setVisibility(View.GONE);
            mNotification_highligh.setVisibility(View.GONE);
            mSetting_highligh.setVisibility(View.GONE);

        }else if (id == R.id.rl_news){

            mMyCard.setSelected(false);
            mNews.setSelected(true);
            mNotification.setSelected(false);
            mSetting.setSelected(false);

            mMyCard_highligh.setVisibility(View.GONE);
            mNews_highligh.setVisibility(View.VISIBLE);
            mNotification_highligh.setVisibility(View.GONE);
            mSetting_highligh.setVisibility(View.GONE);

        }else if (id == R.id.rl_notification){

            mMyCard.setSelected(false);
            mNews.setSelected(false);
            mNotification.setSelected(true);
            mSetting.setSelected(false);

            mMyCard_highligh.setVisibility(View.GONE);
            mNews_highligh.setVisibility(View.GONE);
            mNotification_highligh.setVisibility(View.VISIBLE);
            mSetting_highligh.setVisibility(View.GONE);

        }else if (id == R.id.rl_setting){

            mMyCard.setSelected(false);
            mNews.setSelected(false);
            mNotification.setSelected(false);
            mSetting.setSelected(true);

            mMyCard_highligh.setVisibility(View.GONE);
            mNews_highligh.setVisibility(View.GONE);
            mNotification_highligh.setVisibility(View.GONE);
            mSetting_highligh.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position){
            case 0:
                setSelectedTab(R.id.rl_myCard);
               // mainRefreshListener.reloadCard();
                break;
            case 1:
                setSelectedTab(R.id.rl_news);
                // mainRefreshListener.reloadCard();
                break;
            case 2:
                setSelectedTab(R.id.rl_notification);
                // mainRefreshListener.reloadCard();
                break;
            case 3:
                setSelectedTab(R.id.rl_setting);
                // mainRefreshListener.reloadCard();
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
